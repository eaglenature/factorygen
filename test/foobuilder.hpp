#pragma once
#include "foofactory_generated.hpp"

class FooBuilder
{
public:
    FooBuilder(std::size_t value) 
        : foo(FooFactory::GetInstance().create(value))
    {

    }

    FooBuilder& with_bah()
    {
        foo->bah();
        return *this;
    }

    FooBuilder& print_stats()
    {
        std::cout << "FooBuilder print_stats" << std::endl;
        //foo->print_stats();
        return *this;
    }    

    FooBuilder& diagonalize()
    {
        std::cout << "FooBuilder diagonalize" << std::endl;
        //foo->diagonalize();
        return *this;
    }     

    FooBuilder& get_result(std::size_t* value)
    {
        *value = 3;
        //foo->get_result(value);
        return *this;
    }

private:
    std::unique_ptr<FooBase> foo;
};