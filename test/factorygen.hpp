#pragma once

#include <unordered_map>
#include <functional>
#include <memory>

template <typename Base>
class FactoryGen
{
public:
    using BasePointer = std::unique_ptr<Base>;
    using Factory = std::function<BasePointer()>;
    using FactoryStore = std::unordered_map<std::size_t, Factory>;

    class Stored {};

    Stored store(std::size_t size, Factory&& factory)
    {        
        factories.emplace(size, std::move(factory));
        return Stored();
    }

    BasePointer create(std::size_t size)
    {
        auto it = factories.find(size);
        if (it != factories.end()) return it->second();
        throw size;
    }

    static FactoryGen& GetInstance()
    {
        static FactoryGen factory;
        return factory;
    }

private:
    FactoryGen() = default;
    FactoryStore factories;
};


#define FACTORYGEN_REGISTER_FACTORY(Type, BaseType)                     \
using Type##Factory = FactoryGen<BaseType>;


#define FACTORYGEN_STORE_INSTANTIATION(Type, BaseType, Size)            \
class Type##_##Size : public FactoryGen<BaseType>                       \
{                                                                       \
    static FactoryGen<BaseType>::Stored stored;                         \
};                                                                      \
FactoryGen<BaseType>::Stored Type##_##Size::stored =                    \
    FactoryGen<BaseType>::GetInstance().store(Size, []                  \
        { return FactoryGen<BaseType>::BasePointer(new Type<Size>());})

