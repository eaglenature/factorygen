#pragma once

#include <iostream>
#include "foobase.hpp"

namespace fspace
{
namespace impl
{
namespace detail
{

template <std::size_t N>
struct Foo : public FooBase
{
    static const std::size_t value = N;

    Foo() {}

    void bah() override
    {
        std::cout << "Foo::bah(): " << value << std::endl;
    }
};

}
}
}