// Test:
// & cd factorygen
// $ g++ -std=c++11 test/main.cpp && ./a.out
// $ python factorygen.py --help
// $ python factorygen.py ./test/ fspace::FooBase fspace::impl::detail::Foo foo.hpp 2 1026
// $ g++ -std=c++11 test/main.cpp && ./a.out

#include <iostream>
#include <vector>
#include "foofactory_generated.hpp"

int main()
{
    std::vector<std::size_t> values = {2, 4, 6, 13, 22, 133, 64, 1011, 1024, 1025};

    for (auto value : values)
    {
        try
        {
            auto foo = FooFactory::GetInstance().create(value);
            foo->bah();
        }
        catch (std::size_t size)
        {
            std::cout << "Failed to create unregistered object of size: " << size << std::endl;
        }
    }
}
