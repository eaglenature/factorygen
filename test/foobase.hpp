#pragma once

namespace fspace
{

struct FooBase
{
    virtual ~FooBase() = default;
    virtual void bah() = 0;
};

}
