import os
import argparse

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("destination", help="destination of generated file")
    parser.add_argument("base_class_name", help="name of base class")
    parser.add_argument("class_name", help="name of class")
    parser.add_argument("class_header_file", help="header file where class is declared")  
    parser.add_argument("range_min", type=int, help="min range value")
    parser.add_argument("range_max", type=int, help="max range value")
    return parser.parse_args() 

def add_empty_line(f):
    f.write("\n")

def add_header(f, args):
    f.write("#pragma once\n")
    f.write("#include \"factorygen.hpp\"\n")    
    header_file = "#include \"" + args.class_header_file + "\"\n"
    header_file += "using " + args.base_class_name + ";\n"
    header_file += "using " + args.class_name + ";\n"
    f.write(header_file)    

def add_register_factory(f, args):
    factory_register = "FACTORYGEN_REGISTER_FACTORY(" \
        + args.class_name.split("::")[-1] + ", " \
        + args.base_class_name.split("::")[-1] \
        + ");\n"
    f.write(factory_register)    

def add_store_instantiation(f, args, size):
    factory_store = "FACTORYGEN_STORE_INSTANTIATION(" \
        + args.class_name.split("::")[-1] + ", " \
        + args.base_class_name.split("::")[-1] + ", " \
        + str(size) \
        + ");\n"
    f.write(factory_store)

def factorygen():
    args = parse_arguments()

    generated_file_name = args.class_header_file.split(".")[0] + "factory_generated.hpp"
    f = open(os.path.join(args.destination, generated_file_name), "w")

    add_header(f, args)
    add_empty_line(f)
    add_register_factory(f, args)
    add_empty_line(f)

    for i in range(args.range_min, args.range_max + 1, 2):
        add_store_instantiation(f, args, i)

    add_empty_line(f)

if __name__ == "__main__":
    factorygen()
    